public class Calculator{
	public static double add (int num1, int num2){
		double num3 = num1 + num2;
		return num3;
	}
	
	public static double subtract (int num1, int num2){
		double num3 = num1 - num2;
		return num3;
	}
	
	public  double multiply (int num1, int num2){
		double num3 = num1 * num2;
		return num3;
	}
	
	public  double divide (int num1, int num2){
		double num3 = num1 / num2;
		return num3;
	}
}

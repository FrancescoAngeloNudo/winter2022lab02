public class MethodsTest{
	public static void main(String[] args){
		int x = 3;
		double num = 9.9;
		System.out.println(x);
		methodNoInputNoReturn();
		System.out.println(x);
		methodOneInputNoReturn(x);
		System.out.println(x);
		methodTwoInputNoReturn(x,num);
		int z = methodNoInputReturnInt();
		System.out.println(z);
		int first = 10;
		int second = 11;
		double printRootNums = sumSquareRoot(first, second);
		System.out.println(printRootNums);
		String s1 = "java";
		String s2 = "programming";
		System.out.println(s1.length());
		System.out.println(s2.length());
		System.out.println(SecondClass.addOne(50));
		SecondClass sc = new SecondClass();
		System.out.println(sc.addTwo(50));
		
	}
	
	public static void methodNoInputNoReturn(){
		System.out.println("I am in a method that takes no input and returns nothing.");
		int x = 50;
		System.out.println(x);
	}
	
	public static void methodOneInputNoReturn(int x){
		x = x-5;
		System.out.println("Inside the method one input no return.");
	}
	
	public static void methodTwoInputNoReturn(int x,double num){
		System.out.println(x);
		System.out.println(num);
	}
	public static int methodNoInputReturnInt(){
		int num2 = 10 - 5;
		return num2;
	}
	
	public static double sumSquareRoot(int first, int second){
		double rootNums = Math.sqrt(first + second);
		return rootNums;
	}
}
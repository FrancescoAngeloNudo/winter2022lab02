import java.util.Scanner;

public class PartThree{
	public static void main (String[]args){
		Scanner scan = new Scanner(System.in);
		int num1 = scan.nextInt();
		int num2 = scan.nextInt();
		System.out.println(Calculator.add(num1, num2));
		System.out.println(Calculator.subtract(num1, num2));
		Calculator cal = new Calculator();
		System.out.println(cal.multiply(num1, num2));
		System.out.println(cal.divide(num1,num2));
	}
}